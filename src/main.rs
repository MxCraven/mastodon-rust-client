/*
 * This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 * */

//Crates
extern crate elefren;
extern crate gtk;
extern crate serde_json;

//Elefren
use elefren::prelude::*;
use elefren::helpers::toml; // requires `features = ["toml"]`
use elefren::helpers::cli;

//GTK
use gtk::prelude::*;

//serde
use serde_json::{Value};

//Standard Libs
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;

// -> Result<(), Box<Error>>
fn main() -> Result<(), Box<Error>> {
    let mastodon = if let Ok(data) = toml::from_file("mastodon-data.toml") {
	Mastodon::from(data)
    } else {
	register()?
    };

    let you = mastodon.verify_credentials()?;

    println!("{:#?}", you);
	
	let mut file = File::create("tl.json")?;
	
	
	let tl = mastodon.get_home_timeline()?;
	//println!("{:#?}", tl);
	
	let mut tl_string: String = String::new();
	tl_string = format!("{:#?}", tl);
	
	write!(&file, "{:#?}", tl);
	
	load_ui(&tl_string);
	get_toot(&tl_string);
	
    Ok(())
}

fn load_ui(tl_string: &String) {
	
	if gtk::init().is_err() {
	println!("Failed to initialize GTK.");
	return;
    }
	
	let glade_src = include_str!("toot.glade");
	let builder = gtk::Builder::new_from_string(glade_src);
	
	let window: gtk::Window = builder.get_object("main_window").unwrap();
	
	let lbl_username: gtk::Label = builder.get_object("lbl_username").unwrap();
	let lbl_content: gtk::Label = builder.get_object("lbl_content").unwrap();
	
	window.show_all();
	
	window.connect_delete_event(|_, _| {
	gtk::main_quit();
	Inhibit(false)
    });
	
	
	gtk::main();
}

//Take in JSON for 1 status, return gtk object
fn setup_toot() {
	
}

//Return the JSON for 1 toot
fn get_toot(tl_string: &String) -> Result<(), Box<Error>> {
	//Parse JSON
	
	
	Ok(())
}

fn register() -> Result<Mastodon, Box<Error>> {
    let registration = Registration::new("https://radical.town")
				    .client_name("masto-rust")
				    .build()?;
    let mastodon = cli::authenticate(registration)?;

    // Save app data for using on the next run.
    toml::to_file(&*mastodon, "mastodon-data.toml")?;

    Ok(mastodon)
}
