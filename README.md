# A Mastodon Client Written In Rust

This client is an attempt by me to make a Mastodon client for the Pi, or just 
a low resourse client since all Mastodon clients I can find are either web 
or Electron (desktop-web). 

I haven't really got a clue what I'm doing, so any help would be appreciated.

So far I have got data from the server, but I am struggling with extracting the 
JSON to be used in the GUI. After I've got that going I'm sure I'll find it 
hard to get the list of toots loaded in the GUI. I am very new to Rust as a 
language, and I've never written anything this big before... I'd have done it 
in Python, but that would have entirely defeated the idea of lightweight and 
running on the Raspberry Pi. 

Thanks for any help you can give :D
